var router = require('express').Router();

var jwt = require('jwt-simple');

// split up route handling

router.use('/auth', require('./AuthController'));
router.use('/events', require('./EventsController'));

module.exports = router;