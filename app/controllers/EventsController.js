var router = require('express').Router();
var passport = require('passport');
var jwt = require('jwt-simple');
var config = require ('../../config/database');
var User = require ('../models/User');
var Event = require ('../models/Event');
var mongoose = require('mongoose');
require('../../config/passport')(passport);

router.use(passport.initialize());

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

/* !!!!!! VERIFIER LES DROIT DE PROPRIETE SUR LES EVENTS POUR EDIT DELETE !!!!!!!!*/


// api/events/:id
router.get('/:id', function(req, res, next) {
  //  faire validation request params
  Event.findOne({_id:req.params.id}, function(err, event){
    if(err){
      res.status(409).json({success: false, msg : 'erreur', error : err});
      return next(err);
    }
    res.json({success: true, event : event});
  });
});

// api/events
router.get('/', passport.authenticate('jwt', { session: false}), function(req, res, next) {
  
  var user = getToken(req.headers);
  if(user){
    user = jwt.decode(user, config.secret);
    owner_id = mongoose.Types.ObjectId(user._id);
;
  }

  Event.find({ _ownerid: { $ne: owner_id }}, function(err, events){

    if(err){
      res.status(409).json({success: false, msg : 'erreur', error : err});
      return next(err);
    }
    res.json({success: true, events : events});

  });
});


router.post('/', /*passport.authenticate('jwt', { session: false}),*/ function(req, res, next) {

  var Evt = new Event({

    _ownerid: req.body._ownerid,
    title: req.body.title,
    duration: req.body.duration,
    date: Date(req.body.date),
    sports : req.body.sports,
    participants : req.body.participants || [{"username":"adam"}],
    description : req.body.description

  });

  Evt.save(function(err){
    if(err){
      res.status(409).json({success: false, error: err});
      return next(err);
    }
    res.json({success: true, msg: 'insert ok', error : err});        
  });
  
});

// api/products/:id
router.put('/', function(req, res, next) {
  
  Event.findOne({_id: req.body._id}, function(err, evt){

    if(err) return next(err);
    
    evt.participants = req.body.participants;
    evt.save();
    res.json({ success:true, evt : evt});
  });

});



// api/products/:id
router.delete('/:id', function(req, res, next) {

  Event.remove({_id:req.params.id}, function(err){

    if(err){
      res.status(409).json({success: false, msg : 'erreur', error : err});
      return next(err);
    }
    res.json({ success: true, msg : 'event correctement supprimé' });
  });
});

module.exports = router;




