var router = require('express').Router();
var config = require ('../../config/database');
var jwt = require('jwt-simple');
var User = require ('../models/User');

router.post('/sign-up', function(req, res, next){

		if(!req.body.email || !req.body.password || !req.body.name){
			// renvoyer une erreur et modifier le code http
			res.json({success : false, msg : 'please provide email AND password'});
		} else {

			console.log(req.body);

			var newUser = new User({
				name : req.body.name,
				email : req.body.email,
				password : req.body.password
			});

			newUser.save(function(err){
				if(err){ 
					res.status(409).json({success : false, msg : 'error occured while registration', error : err});
					return next(err);
				}
				res.json({success: true, msg: 'Successful created new user.', token :'JWT ' +  jwt.encode(newUser, config.secret), user: newUser });
			});
		}
	});

	router.post('/sign-in', function(req, res){

		User.findOne({
			email : req.body.email
		}, 
		function(err, user){
			if(err) throw err;

			if(!user) {
				res.send({success : false, msg : 'Authentication failed. User not found.'});
			} else {

				user.comparePassword(req.body.password, function(err, isMatch){
					if(err) res.send({success : false, msg : 'Authentication failed. User not found.'});

					if(isMatch && !err) {
					// if user is found and password is right create a token
					var token = jwt.encode(user, config.secret);
          			// return the information including token as JSON
          			res.json({success: true, token: 'JWT ' + token, user: user});
          		} else {
          			res.send({success : false, msg : 'Authentication failed. Wrong password.'});
          		}
          	});
			}
		});
	});
module.exports = router;

/*var User = require ('../models/User');

module.exports.set = function(app) {

	app.post('/sign-up', function(req, res){

		if(!req.body.name || !req.body.password){

			// renvoyer une erreur et modifier le code http
			res.json({success : false, msg : 'please provide username AND password'});
		} else {
			var newUser = new User({
				name : req.body.name,
				email : req.body.email,
				password : req.body.password
			});

			newUser.save(function(err){
				if(err) 
					res.json({success : false, msg : 'error occured while registration', error : err});
				res.json({success: true, msg: 'Successful created new user.'});
			});
		}

	});

	app.post('/sign-in', function(req, res){

		User.findOne({
			email : req.body.email
		}, 
		function(err, user){
			if(err) throw err;

			if(!user) {
				res.send({success : false, msg : 'Authentication failed. User not found.'});
			} else {

				user.comparePassword(req.body.password, function(err, isMatch){
					if(err) res.send({success : false, msg : 'Authentication failed. User not found.'});

					if(isMatch && !err) {
					// if user is found and password is right create a token
					var token = jwt.encode(user, config.secret);
          			// return the information including token as JSON
          			res.json({success: true, token: 'JWT ' + token});
          		} else {
          			res.send({success : false, msg : 'Authentication failed. Wrong password.'});
          		}
          	});
			}
		});
	});
}*/