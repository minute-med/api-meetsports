var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require ('morgan');
var mongoose = require ('mongoose');
var passport = require ('passport');
var config = require ('../config/database');
var User = require ('./models/User');
var port = process.env.PORT || 4242;
var jwt = require('jwt-simple');


// get(& decode ?) request parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// log to console
app.use(morgan('dev'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, Authorization, X-Requested-With, Content-Type, Accept");
  next();
});

// app.use(passport.initialize());

mongoose.connect(config.database);

// init passport package with application
// require('../config/passport')(passport);

/************************** TEMP ROUTES & Action **************************/

app.use('/api', require('./controllers'));


/************** AUTH ********************/

// var AuthController = require('./controllers/AuthController')(app);
// var EventsController = require('./controllers/EventsController')(app);



/************** EVENT ********************/

/*// List all events
app.get('event', function(req, res){

});

// get 1 event
app.get('event/:id', function(req, res){

});

// create an event
app.post('event', function(req, res){});

// update an event
app.put('event/:id', function(req, res){});

// delete an event
app.delete('event/:id', function(req, res){});
*/
/***********************************************************/


/*app.get('/events', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      email: decoded.email
    }, function(err, user) {
        if (err) throw err;
 
        if (!user) {
          return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
          res.json({success: true, msg: 'you can have events ' + user.name + '!'});
        }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
});
*/ 
getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

// connect the api routes under /api/*
// app.use('/api', route);


app.listen(port);

console.log('Server running at: http://localhost:' + port);