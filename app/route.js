module.exports = {

	events : function(app){

		app.get('/events', function(req, res){

			Event.find(null, function(err, events){
				if(!err)
					res.json({success : true, events : events});
				else
					res.json({success : false, events : []});
			});
		});
	},

	Auth : function(app){


		app.post('/sign-up', function(req, res){

			if(!req.body.name || !req.body.password){

			// renvoyer une erreur et modifier le code http
			res.json({success : false, msg : 'please provide username AND password'});
		} else {
			var newUser = new User({
				name : req.body.name,
				email : req.body.email,
				password : req.body.password
			});

			newUser.save(function(err){
				if(err) 
					res.json({success : false, msg : 'error occured while registration', error : err});
				res.json({success: true, msg: 'Successful created new user.'});
			});
		}

	});

		app.post('/sign-in', function(req, res){

			User.findOne({
				email : req.body.email
			}, 
			function(err, user){
				if(err) throw err;

				if(!user) {
					res.send({success : false, msg : 'Authentication failed. User not found.'});
				} else {

					user.comparePassword(req.body.password, function(err, isMatch){
						if(err) res.send({success : false, msg : 'Authentication failed. User not found.'});

						if(isMatch && !err) {
							// if user is found and password is right create a token
							var token = jwt.encode(user, config.secret);
				  			// return the information including token as JSON
				  			res.json({success: true, token: 'JWT ' + token});
				  		} else {
				  			res.send({success : false, msg : 'Authentication failed. Wrong password.'});
				  		}
	          		});
				}
			});
		});
	}

}