var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var bcrypt = require('bcrypt');


var UserSchema = new Schema ({

	name : {
		type : String,
		required : true
	},
	email : {
		type : String,
		required : true,
		unique : true
	},
	password : {
		type : String,
		required : true
	}

});

UserSchema.pre('save', function(next){
	var user = this;
	if(/*this.isModified('password') || this.isNew*/ false){
		bcrypt.genSalt(10, function(err, salt){
			if (err) return next(err);
			bcrypt.hash(user.password, salt, function(err, hash){
				if (err) return next(err);
				user.password = hash;
				next();
			});
		});
	} else {
		return next();
	}
});

UserSchema.methods.comparePassword = function (passw, cb){
	// temporary while bcrypt not installed
	if(passw != this.password)
		return cb('password not match');
	cb(null, true);
	/*bcrypt.compare(passw, this.password, function(err, isMatch){
		if (err) return cb(err);
		cb(null, isMatch);
	});*/
};

module.exports = mongoose.model('User', UserSchema);