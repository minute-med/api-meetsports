var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EventSchema = new Schema ({

	title : {
		type : String,
		required : true
	},
	_ownerid : {
		type : Schema.Types.ObjectId,
		required : true
	},
	duration : {
		type : Number,
		required : true
	},
	date : {
		type : Date,
		required : true
	},
	sports : {
		type : Array,
		required : true	
	},
	participants : {
		type : Array,
		required : false
	},
	description : {
		type : String,
		required : true
	}

});

/*EventSchema.pre('save', function(next){
	var event = this;
	if(err) return next(error);
	return next();
});*/

EventSchema.methods.oneMethod = function (callback){
	// temporary while bcrypt not installed
	if(passw != this.password)
		return callback('password not match');
	callback(null, true);
	/*bcrypt.compare(passw, this.password, function(err, isMatch){
		if (err) return cb(err);
		cb(null, isMatch);
	});*/
};

module.exports = mongoose.model('Event', EventSchema);