var JwtStrategy = require ('passport-jwt').Strategy;

var User = require ('../app/models/User.js');
var dbConfig = require ('../config/database');


// mongoose.connect(config.database);

module.exports = function(passport){

	var opts = [];
	opts.secretOrKey = dbConfig.secret;
	passport.use(new JwtStrategy(opts, function(jwt_payload, done){
		User.findOne({id:jwt_payload.id}, function(err, user){
			if(err) return done(err, false);
			if(user) done(null, user);
			else done(err, false);
		});
	}));


};